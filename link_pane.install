<?php

/**
* Implementation of hook_schema().
*/
function link_pane_schema() {
  $schema['link_pane'] = array(
    'description' => t('Table storing link_pane link preset definitions.'),
    'export' => array(
      'key' => 'name',
      'identifier' => 'preset', // Exports will be defined as $preset
      'default hook' => 'default_link_pane_preset',  // Function hook name.
      'api' => array(
        'owner' => 'link_pane',
        'api' => 'default_link_pane_presets',  // Base name for api include files.
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),
    'fields' => array(
      'name' => array(
        'type' => 'varchar',
        'length' => '255',
        'description' => 'Unique ID for presets. Used to identify them programmatically.',
      ),
      'pid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Primary ID field for the table. Not used for anything except internal lookups.',
        'no export' => TRUE, // Do not export database-only keys.
      ),
      'admin_title' => array(
        'type' => 'varchar',
        'length' => '128',
        'description' => 'The administrative title of the link.',
      ),
      'description' => array(
        'type' => 'varchar',
        'length' => '255',
        'description' => 'A human readable name of a preset.',
      ),
      'text' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'Link text.',
      ),
      'path' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'Link path.',
      ),
      'query' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'Query string to append to the URL.',
      ),
      'fragment' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'A fragment identifier (named anchor) to append to the URL.',
      ),
      // From http://api.drupal.org/api/drupal/includes--common.inc/function/url/7
      'absolute' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
        'description' => 'Defaults to FALSE. Whether to force the output to be an absolute link (beginning with http:).',
      ),
      'alias' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
        'description' => 'Defaults to FALSE. Whether the given path is a URL alias already.',
      ),
      'external' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
        'description' => 'Whether the given path is an external URL.',
      ),
      'https' => array(
        'type' => 'int',
        'size' => 'tiny',
        'default' => 0,
        'description' => 'Whether this URL should point to a secure location. If not defined, the current scheme is used, so the user stays on http or https respectively.',
      ),
      // From http://www.w3.org/TR/html4/struct/links.html#adef-name-A
      'id' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'ID attribute of link tag.',
      ),
      'class' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'Class attribute of link tag, classes separated by space.',
      ),
      'title' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'Title of link tag.',
      ),
      'target' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'Target of link tag. Depreciated in HTML5.',
      ),
      'tabindex' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'Tab index of link tag.',
      ),
      'accesskey' => array(
        'type' => 'text',
        'size' => 'big',
        'description' => 'Access key of link tag.',
      ),
     ),
    'primary key' => array('pid'),
    'unique keys' => array(
      'name' => array('name'),
    ),
  );
  return $schema;
}

///**
//* Implementation of hook_install().
//*/
//function link_pane_install() {
//  drupal_install_schema('link_pane');
//}